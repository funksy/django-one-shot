from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm
import operator

# Create your views here.
def todo_list_list(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list_list,
    }
    return render(request, "todos/todo_list_list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    completed_tasks = []
    tasks_w_due_date = []
    tasks_wout_due_date = []
    for task in todo_list.items.all():
        if task.is_completed:
            completed_tasks.append(task)
        else:
            if task.due_date == None:
                tasks_wout_due_date.append(task)
            else:
                tasks_w_due_date.append(task)
    incomplete_tasks = sorted(tasks_w_due_date, key=operator.attrgetter('due_date'))
    incomplete_tasks += tasks_wout_due_date
    context = {
        "todo_list": todo_list,
        "completed_tasks": completed_tasks,
        "incomplete_tasks": incomplete_tasks,
    }
    return render(request, "todos/todo_list_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            new_list = form.save()
            return redirect("todo_list_detail", new_list.id)

    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_create.html", context)


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id)
    else:
        form = TodoListForm(instance=todo_list)
    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_update.html", context)


def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    else:
        form = TodoListForm(instance=todo_list)
    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_delete.html", context)



def todo_item_create(request):
    todo_list_id = request.GET.get('todo_list')
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            new_item = form.save()
            return redirect("todo_list_detail", new_item.list.id)
    else:
        if todo_list_id:
            form = TodoItemForm(initial={'list':todo_list_id})
        else:
            form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/todo_item_create.html", context)

def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    print(request.POST)
    if request.method == "POST":
        if "update" in request.POST:
            form = TodoItemForm(request.POST, instance=todo_item)
            if form.is_valid():
                form.save()
                return redirect("todo_list_detail", todo_item.list.id)
        if "delete" in request.POST:
            todo_item.delete()
            return redirect("todo_list_detail", todo_item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)
    context = {
        "form": form,
    }
    return render(request, "todos/todo_item_update.html", context)
